'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('sass', function () {
    return gulp.src('./static/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./static/css'));
});

gulp.task('watch', function () {
    gulp.watch('./static/scss/**/*', ['sass']);
});

gulp.task('default', ['sass', 'watch']);
