from django.conf.urls import url

from public.views import HomeView, PublishedPageView


urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^(?P<slug>[\w_\-]+)/$', PublishedPageView.as_view(), name='page'),
]
