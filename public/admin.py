from django.contrib import admin
from public.models import HomePage, PublicPage, Photo


class PhotoAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'file')


admin.site.register(HomePage)
admin.site.register(PublicPage)
admin.site.register(Photo, PhotoAdmin)
