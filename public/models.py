from django.db import models


class HomePage(models.Model):
    def __str__(self):
        return 'Home page'

    def promoted_photos(self):
        return Photo.objects.filter(in_home_page=True)


class PublicPage(models.Model):
    title = models.CharField(max_length=128, blank=True)
    slug = models.CharField(max_length=128, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.title

    def photos(self):
        return Photo.objects.filter(page=self)


class Photo(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField(blank=True)
    file = models.ImageField(null=True)
    in_home_page = models.BooleanField(default=False)
    page = models.ForeignKey('PublicPage', null=True, blank=True)

    def __str__(self):
        return self.title
