from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic import ListView

from public.models import HomePage, PublicPage


class NavigationMixin(object):
    def get_context_data(self, **kwargs):
        ctx = super(NavigationMixin, self).get_context_data(**kwargs)
        ctx['pages'] = PublicPage.objects.all()
        return ctx


class HomeView(NavigationMixin, DetailView):
    model = HomePage
    template_name = 'public/home.html'
    context_object_name = 'homepage'

    def get_object(self, queryset=None):
        try:
            return self.model.objects.get()
        # Create a homepage if it doesn't exist.
        except HomePage.DoesNotExist:
            return self.model.objects.create()
        # Make sure there's only one homepage.
        except HomePage.MultipleObjectsReturned:
            self.model.objects.all().delete()
            return self.model.objects.create()


class PublishedPageView(NavigationMixin, DetailView):
    template_name = 'public/public_page.html'
    model = PublicPage
    context_object_name = 'page'

    def get_queryset(self):
        return self.model.objects.filter(slug=self.kwargs.get('slug'))
